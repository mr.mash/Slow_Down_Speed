package sds.picmash.com.slowdownspeed.mvp.base;

import android.app.Activity;

import com.arellomobile.mvp.MvpPresenter;
import com.arellomobile.mvp.MvpView;

import sds.picmash.com.slowdownspeed.SlowDownSpeed;
import sds.picmash.com.slowdownspeed.di.components.DaggerPresenterComponent;
import sds.picmash.com.slowdownspeed.di.components.PresenterComponent;
import sds.picmash.com.slowdownspeed.di.modules.PresenterModule;

public abstract class BaseMvpPresenter<V extends MvpView> extends MvpPresenter<V> {

    private static final String TAG = "BaseMvpPresenter";
    protected Activity activity;

    protected PresenterComponent component;

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
    }

    private void initComponent(Activity activity) {
        component = DaggerPresenterComponent
                .builder()
                .applicationComponent(SlowDownSpeed.get(activity).getApplicationComponent())
                .presenterModule(new PresenterModule())
                .build();
    }

    public PresenterComponent getComponent() {
        return component;
    }

    protected void onViewCreated (Activity activity) {
        this.activity = activity;
        initComponent(activity);
        injectComponennt();
    }

    protected abstract void injectComponennt();
}
