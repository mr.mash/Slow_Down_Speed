package sds.picmash.com.slowdownspeed.di.components;


import android.content.Context;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.RequestQueue;

import java.nio.file.AccessMode;

import dagger.Component;
import sds.picmash.com.slowdownspeed.di.modules.ActivityModule;
import sds.picmash.com.slowdownspeed.di.modules.ApplicationModule;
import sds.picmash.com.slowdownspeed.di.scopes.ApplicationScope;

@ApplicationScope
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {

    RequestQueue getRequestQueue();
    Cache getCache();
    Network getNetwork();
    Context getContext();

}
