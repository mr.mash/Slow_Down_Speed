package sds.picmash.com.slowdownspeed.mvp.ui.dialogs;

import android.content.Context;
import android.view.View;
import android.widget.Button;

import butterknife.BindView;
import sds.picmash.com.slowdownspeed.R;

public class AddPointDialog extends BaseDialog {

    @BindView(R.id.confirm_btn)
    protected Button confirmBtn;
    @BindView(R.id.cancel_btn)
    protected Button cancalBtn;

    public AddPointDialog(Context context) {
        super(context);
    }

    public AddPointDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected int getLayout() {
        return R.layout.view_add_point;
    }

    @Override
    protected View getConfirmButton() {
        return confirmBtn;
    }

    @Override
    protected View getCancelButton() {
        return cancalBtn;
    }
}
