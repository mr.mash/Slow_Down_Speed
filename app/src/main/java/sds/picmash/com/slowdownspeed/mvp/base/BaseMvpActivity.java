package sds.picmash.com.slowdownspeed.mvp.base;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import com.arellomobile.mvp.MvpAppCompatActivity;

import butterknife.ButterKnife;
import sds.picmash.com.slowdownspeed.SlowDownSpeed;
import sds.picmash.com.slowdownspeed.di.components.ActivityComponent;
import sds.picmash.com.slowdownspeed.di.components.DaggerActivityComponent;
import sds.picmash.com.slowdownspeed.di.components.DaggerPresenterComponent;
import sds.picmash.com.slowdownspeed.di.components.PresenterComponent;
import sds.picmash.com.slowdownspeed.di.modules.ActivityModule;

public abstract class BaseMvpActivity extends MvpAppCompatActivity {

    protected ActivityComponent component;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        ButterKnife.bind(this);
        initViews();
        initComponent(initActivityComponent());
        injectComponent();
    }

    private void initComponent (Activity activity) {
        component =  DaggerActivityComponent
                .builder()
                .applicationComponent(SlowDownSpeed.get(activity).getApplicationComponent())
                .activityModule(new ActivityModule(activity))
                .build();
    }

    public ActivityComponent getComponent() {
        return component;
    }

    protected abstract int getLayout();
    protected abstract View getRootView();
    protected abstract void initViews ();
    protected abstract Activity initActivityComponent ();
    protected abstract void injectComponent();
}
