package sds.picmash.com.slowdownspeed.di.modules;

import android.location.Geocoder;

import com.android.volley.RequestQueue;
import com.google.android.gms.maps.model.LatLng;

import dagger.Module;
import dagger.Provides;
import sds.picmash.com.slowdownspeed.di.scopes.PresenterScp;

@Module
public class PresenterModule {

    @Provides
    @PresenterScp
    public LatLng provideGeocoder () {
        return new LatLng(0, 0);
    }
}
