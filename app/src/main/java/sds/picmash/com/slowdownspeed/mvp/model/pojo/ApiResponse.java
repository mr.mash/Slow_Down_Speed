package sds.picmash.com.slowdownspeed.mvp.model.pojo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ApiResponse {

    private int code;
    private String message;

    private JSONObject rootJSON;
    private JSONArray responseJSONarray;

    public ApiResponse (String stringJson)  {
        try {
            setRootJSON(stringJson);
            setCode(getRootJSON().getInt("code"));
            setMessage(getRootJSON().getString("message"));
            setResponseJSONarray();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void setMessage(String message) {
        this.message = message;
    }

    private void setCode(int code) {
        this.code = code;
    }

    private void setRootJSON (String  stringJson) throws JSONException {
        rootJSON = new JSONObject(stringJson);
    }

    private void setResponseJSONarray () throws JSONException {
        responseJSONarray = rootJSON.getJSONArray("Response");
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public boolean isSuccessfull () {
        return getCode() == 200;
    }

    public JSONObject getRootJSON() {
        return rootJSON;
    }

    public JSONArray getResponseJSONarray() {
        return responseJSONarray;
    }
}
