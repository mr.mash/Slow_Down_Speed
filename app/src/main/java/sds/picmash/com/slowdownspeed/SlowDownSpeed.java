package sds.picmash.com.slowdownspeed;

import android.app.Activity;
import android.app.Application;

import com.android.volley.RequestQueue;

import sds.picmash.com.slowdownspeed.di.components.ApplicationComponent;
import sds.picmash.com.slowdownspeed.di.components.DaggerApplicationComponent;
import sds.picmash.com.slowdownspeed.di.modules.ApplicationModule;

public class SlowDownSpeed extends Application {

    private static final String TAG = "SlowDownSpeed";

    private ApplicationComponent applicationComponent;
    
    public static SlowDownSpeed get (Activity activity) {
        return (SlowDownSpeed) activity.getApplication();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();

    }

    public ApplicationComponent getApplicationComponent () {
        return applicationComponent;
    }
}
