package sds.picmash.com.slowdownspeed.di.components;

import dagger.Component;
import sds.picmash.com.slowdownspeed.di.modules.ActivityModule;
import sds.picmash.com.slowdownspeed.di.scopes.ActivityScp;
import sds.picmash.com.slowdownspeed.mvp.ui.home.MapActivity;
import sds.picmash.com.slowdownspeed.mvp.ui.profile.SignInActivity;

@ActivityScp
@Component(modules = ActivityModule.class, dependencies = ApplicationComponent.class)
public interface ActivityComponent {

    void inject(MapActivity target);
    void inject(SignInActivity target);

}
