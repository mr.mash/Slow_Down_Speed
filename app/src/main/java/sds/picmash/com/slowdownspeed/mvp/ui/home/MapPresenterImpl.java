package sds.picmash.com.slowdownspeed.mvp.ui.home;

import android.app.Activity;
import android.location.Geocoder;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.arellomobile.mvp.InjectViewState;
import com.google.android.gms.maps.model.LatLng;

import javax.inject.Inject;

import sds.picmash.com.slowdownspeed.mvp.base.BaseMvpPresenter;
import sds.picmash.com.slowdownspeed.mvp.ui.profile.SignInActivity;

@InjectViewState
public class MapPresenterImpl extends BaseMvpPresenter<MapView> {

    private static final String TAG = "MapPresenterImpl";

    @Inject
    protected LatLng latLng;
    @Inject
    protected RequestQueue requestQueue;


    @Override
    protected void onViewCreated(Activity activity) {
        super.onViewCreated(activity);
    }

    @Override
    protected void injectComponennt() {
        getComponent().inject(this);
    }

    public void onAddPointConfirmClick () {
        //
    }
}
