package sds.picmash.com.slowdownspeed.di.components;

import android.transition.SidePropagation;

import com.android.volley.RequestQueue;

import dagger.Component;
import dagger.Provides;
import sds.picmash.com.slowdownspeed.di.modules.PresenterModule;
import sds.picmash.com.slowdownspeed.di.scopes.PresenterScp;
import sds.picmash.com.slowdownspeed.mvp.ui.home.MapPresenterImpl;
import sds.picmash.com.slowdownspeed.mvp.ui.profile.SignInPresenterImpl;

@PresenterScp
@Component(modules = PresenterModule.class, dependencies = ApplicationComponent.class )
public interface PresenterComponent {

    void inject (MapPresenterImpl target);
    void inject (SignInPresenterImpl target);

}
