package sds.picmash.com.slowdownspeed.mvp.ui.profile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.arellomobile.mvp.presenter.InjectPresenter;

import javax.inject.Inject;

import sds.picmash.com.slowdownspeed.R;
import sds.picmash.com.slowdownspeed.mvp.base.BaseMvpActivity;

public class SignInActivity extends BaseMvpActivity implements SignInView {

    private static final String TAG = "SignInActivity";

    @Inject
    protected RequestQueue requestQueue;

    @InjectPresenter
    SignInPresenterImpl presenter;

    public static void openThis (Context context) {
        context.startActivity(new Intent(context, SignInActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.onViewCreated(this);

        Log.i(TAG, "Request queue is " + requestQueue.toString());
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_sign_in;
    }

    @Override
    protected View getRootView() {
        return null;
    }

    @Override
    protected void initViews() {

    }

    @Override
    protected Activity initActivityComponent() {
        return this;
    }

    @Override
    protected void injectComponent() {
        getComponent().inject(this);
    }
}
