package sds.picmash.com.slowdownspeed.mvp.ui.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;


import butterknife.ButterKnife;

public abstract class BaseDialog extends AlertDialog {

    private DialogCallbacks dialogCallbacks;

    protected BaseDialog(Context context) {
        super(context);
    }

    protected BaseDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        setContentView(getLayout());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        ButterKnife.bind(this);

        getConfirmButton().setOnClickListener(onConfirmClick);
        getCancelButton().setOnClickListener(onCalcelClick);
    }

    protected abstract int getLayout ();
    protected abstract View getConfirmButton ();
    protected abstract View getCancelButton ();

    public void setDialogCallbacks (DialogCallbacks dialogCallbacks) {
        this.dialogCallbacks = dialogCallbacks;
    }

    private View.OnClickListener onConfirmClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            dialogCallbacks.onConfirm();
        }
    };

    private View.OnClickListener onCalcelClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            dialogCallbacks.onCalcel();
        }
    };

    public interface DialogCallbacks {
        void onConfirm();
        void onCalcel();
    }
}
