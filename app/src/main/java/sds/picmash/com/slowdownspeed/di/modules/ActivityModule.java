package sds.picmash.com.slowdownspeed.di.modules;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;

import dagger.Module;
import dagger.Provides;
import sds.picmash.com.slowdownspeed.di.scopes.ActivityScp;

@Module
public class ActivityModule {

    private final Context context;
    private LayoutInflater layoutInflater;

    public ActivityModule(Activity context) {
        this.context = context;
    }

    @Provides
    @ActivityScp
    public LayoutInflater getLayoutInflater () {
        if (layoutInflater == null)
            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        return layoutInflater;
    }

}
