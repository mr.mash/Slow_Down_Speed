package sds.picmash.com.slowdownspeed.mvp.ui.home;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.support.constraint.ConstraintLayout;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import javax.inject.Inject;

import butterknife.BindView;
import info.hoang8f.android.segmented.SegmentedGroup;
import sds.picmash.com.slowdownspeed.R;
import sds.picmash.com.slowdownspeed.mvp.base.BaseMvpActivity;
import sds.picmash.com.slowdownspeed.mvp.ui.dialogs.AddPointDialog;
import sds.picmash.com.slowdownspeed.mvp.ui.dialogs.BaseDialog;
import sds.picmash.com.slowdownspeed.mvp.ui.profile.SignInActivity;

public class MapActivity extends BaseMvpActivity implements MapView, OnMapReadyCallback {

    private static final String TAG = "MapActivity";

    @InjectPresenter
    MapPresenterImpl presenter;

    @Inject
    protected LayoutInflater inflater;
    @Inject
    protected Resources resources;

    @BindView(R.id.root_view)
    protected ConstraintLayout rootView;
    @BindView(R.id.progress_view)
    protected ProgressBar progressView;
    @BindView(R.id.point_types)
    protected SegmentedGroup pointTypes;

    private GoogleMap mMap;
    private AddPointDialog addPointDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initAddPointDialog();
        presenter.onViewCreated(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_map;
    }

    @Override
    protected View getRootView() {
        return rootView;
    }

    @Override
    protected void initViews() {
        //init map
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        initAddPointDialog();

        //init pregress view
        progressView
                .setIndeterminate(true);
        progressView
                .getIndeterminateDrawable()
                .setColorFilter(this.getResources().getColor(R.color.color_green), PorterDuff.Mode.MULTIPLY);

        //init point type picker
        //pointTypes.setTintColor(resources.getColor(R.color.color_green));
    }

    @Override
    protected Activity initActivityComponent() {
        return this;
    }

    @Override
    protected void injectComponent() {
        getComponent().inject(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                showAddPointDialog();
            }
        });
    }

    @Override
    public void showAlert(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setMarker(int lan, int lat, String title) {

    }

    @Override
    public void showAddPointDialog() {
        addPointDialog.show();
    }

    @Override
    public void showProgress() {
        progressView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress () {
        progressView.setVisibility(View.GONE);
    }

    private void initAddPointDialog () {
        this.addPointDialog = new AddPointDialog(this);
        this.addPointDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                addPointDialog.hide();
            }
        });

        addPointDialog.setDialogCallbacks(new BaseDialog.DialogCallbacks() {
            @Override
            public void onConfirm() {
                addPointDialog.cancel();
                addPointDialog = null;
                initAddPointDialog();

                SignInActivity.openThis(MapActivity.this);
            }

            @Override
            public void onCalcel () {
                addPointDialog.hide();
            }
        });
    }
}
