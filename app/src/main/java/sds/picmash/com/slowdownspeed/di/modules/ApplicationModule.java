package sds.picmash.com.slowdownspeed.di.modules;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;

import dagger.Module;
import dagger.Provides;
import sds.picmash.com.slowdownspeed.di.scopes.ApplicationScope;

@Module
public class ApplicationModule {

    private Application application;
    private Context context;

    private RequestQueue requestQueue;
    private Cache cache;
    private Network network;
    private Resources resources;

    public ApplicationModule(Application app) {
        this.application = app;
    }

    @Provides
    @ApplicationScope
    public RequestQueue getRequestQueue() {
        if (requestQueue == null)
            requestQueue = new RequestQueue(getCache(), getNetwork());

        return requestQueue;
    }

    @Provides
    @ApplicationScope
    public Cache getCache() {
        if (cache == null)
            cache = new DiskBasedCache(application.getCacheDir(), 1024 * 1024);

        return cache;
    }

    @Provides
    @ApplicationScope
    public Network getNetwork() {
        if (network == null)
            network = new BasicNetwork(new HurlStack());

        return network;
    }

    @Provides
    @ApplicationScope
    public Context getContext() {
        if (context == null)
            context = application.getApplicationContext();

        return context;
    }

    @Provides
    @ApplicationScope
    public Resources getResources() {
        if (resources == null)
            resources = getContext().getResources();

        return resources;
    }

}
