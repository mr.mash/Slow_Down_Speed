package sds.picmash.com.slowdownspeed.mvp.ui.home;

import android.app.Activity;

import sds.picmash.com.slowdownspeed.mvp.base.MvpActivity;

public interface MapView extends MvpActivity {

    void showAlert (String message);
    void setMarker (int lan, int lat, String title);
    void showAddPointDialog ();

    void showProgress ();
    void hideProgress ();
}
